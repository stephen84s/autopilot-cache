# Autpilot Contacts Cacher

Endpoints available:-

- Create / Update Contact (POST)
- Get Contact (GET)
- Delete Contact (DELETE)

## Caching

Notes about the cacher:-

- The cacher works as a middleware module.
- For GET requests
  - it intercepts the request
  - check if the contact is present in redis
  - If not then
    - Forward to the real API with a decorated response.
    - The decorated response will read the response sent back by the API and add the value in redis.
  - If found then, just retrieve the contact from redis and send back, no need to go to the API.
- For POST requests
  - A decorated response is sent to the API
  - The returned response is intercepted and if it was positive (200 - OK, 201 - Created) then
    - Remove the cached value from redis.
- For DELETE requests
  - Same behaviour as POST

## Running the application

### Environment variables

We need the following environment variables to connect to `redis`.

| *Environment Variable* | *Value* |
| -----------------------| --------|
| REDIS_HOST | Host on which the Redis server is runnning |
| REDIS_PORT | Port on which to connect to the Redis server |
| PORT | Port on which the App should listen for HTTP requests |
| AUTOPILOT_API_KEY | Autopilot API Key |

## Running via Docker

### Prerequisites

- docker

### Steps

You only need to set the `AUTOPILOT_API_KEY` environment variable if running via docker.
To run the application via `docker`, just run the the following command:-

```bash
docker-compose up
```

This should create the docker image locally, download the `redis-alpine` docker image and start the application.

## Running via Node

### Prerequisites for running via node

- node 11.9.0
- yarn
- redis

If you would like to avoid installing a redis instance, just run the following command from the root of the project:-

```bash
docker-compose up redis
```

This will just start the redis instance and expose it on port 6379.

### Download dependencies

The following command will download and install the dependencies.

```bash
yarn
```

### Starting the application

#### Production mode

- Build the application

  ```bash
  yarn build
  ```

- Start the application:-

  ```bash
  yarn start
  ```

  The above should start the application on port 3000. To run on a different port just set the environment variable `PORT`.

#### Development mode

```bash
yarn start.dev
```

This should start the application on port 3000. It will run the build and restart the node server whenever any changes are detected.
