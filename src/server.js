import express from 'express';
import bunyanMiddleWare from 'bunyan-middleware';
import routes from './routes';
import bodyParser from 'body-parser';

const createServer = logger => {
  const app = express();
  app.use(bunyanMiddleWare(logger));
  app.use(bodyParser.json());
  app.use(routes);

  return app;
};

export default createServer;
