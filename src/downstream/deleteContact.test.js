import HTTPStatus from 'http-status';
import fetch from 'node-fetch';
import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';
import deleteContact from './deleteContact';

jest.mock('node-fetch');

describe('deleteContact', () => {
  let oldEnvVars = {};
  const expectedContactId = 'TonyStark';

  beforeAll(() => {
    oldEnvVars = {
      AUTOPILOT_API_KEY: process.env.AUTOPILOT_API_KEY
    };
  });

  afterAll(() => {
    process.env = {...oldEnvVars};
  });

  afterEach(() => jest.resetAllMocks());

  beforeEach(() => {
    process.env.AUTOPILOT_API_KEY = 'SomeRandomKey';
    fetch.mockImplementation(() => ({
      status: HTTPStatus.OK,
    }));
  });

  it('should pass the AUTOPILOT_API_KEY in the header "autopilotapikey" and contactId param', async () => {
    await deleteContact(expectedContactId);
    expect(fetch).toHaveBeenCalledWith(
      `${AUTOPILOT_API_BASE_URL}/v1/contact/${expectedContactId}`, {
        headers: {
          [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY
        },
        method: 'DELETE'
      }
    );
  });

  it('should pass the downstream response back', async () => {
    const result = await deleteContact(expectedContactId);
    expect(result).toEqual(HTTPStatus.OK);
  });
});