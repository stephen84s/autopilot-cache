import HTTPStatus from 'http-status';
import addUpdateContact from './addUpdateContact';
import fetch from 'node-fetch';
import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';

jest.mock('node-fetch');

describe('getContact', () => {
  
  const expectedResp = {
    contact_id: 'SomeRandomID'
  };

  const newContact = {
    contact: {
      FirstName: 'Slarty',
      LastName: 'Bartfast',
      Email: 'a@aol.com'
    }
  };
  let oldEnvVars = {};

  beforeAll(() => {
    oldEnvVars = {
      AUTOPILOT_API_KEY: process.env.AUTOPILOT_API_KEY
    };
  });

  afterAll(() => {
    process.env = {...oldEnvVars};
  });

  afterEach(() => jest.resetAllMocks());

  beforeEach(() => {
    process.env.AUTOPILOT_API_KEY = 'SomeRandomKey';
    fetch.mockImplementation(() => ({
      status: HTTPStatus.OK,
      json: () => Promise.resolve(expectedResp)
    }));
  });

  it('should pass the AUTOPILOT_API_KEY in the header "autopilotapikey" and contactId param', async () => {
    await addUpdateContact(newContact);
    expect(fetch).toHaveBeenCalledWith(
      `${AUTOPILOT_API_BASE_URL}/v1/contact`, {
        headers: {
          [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY,
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(newContact)
      }
    );
  });

  it('should pass the downstream response back', async () => {
    const result = await addUpdateContact(newContact);
    expect(result).toEqual({
      status: HTTPStatus.OK,
      body: expectedResp
    });
  });
});