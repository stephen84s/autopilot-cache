import fetch from 'node-fetch';
import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';
import logger from '../utils/logger';

const addUpdateContact = async contact => {
  logger.info(`Sending contact ${JSON.stringify(contact)}`);
  const resp = await fetch(
    `${AUTOPILOT_API_BASE_URL}/v1/contact`, {
      method: 'POST',
      headers: {
        [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(contact)
    });

  logger.info(`Response from downstream ${resp.status}`);
  
  const body = await resp.json();
  return {
    status: resp.status,
    body
  };
  
};

export default addUpdateContact;