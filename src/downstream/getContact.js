import fetch from 'node-fetch';

import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';

/**
 * Get a contact from auto pilot API using either the ID or email address
 * @param {string} contactId 
 */
const getContact = async contactId => {
  const resp = await fetch(
    `${AUTOPILOT_API_BASE_URL}/v1/contact/${contactId}`, {
      headers: {
        [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY,
        'Content-Type': 'application/json'
      },
      method: 'GET'
    });

  const body = await resp.json();
  return {
    status: resp.status,
    body
  };
};

export default getContact;