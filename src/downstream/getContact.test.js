import HTTPStatus from 'http-status';
import getContact from './getContact';
import fetch from 'node-fetch';
import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';

jest.mock('node-fetch');

describe('getContact', () => {
  
  const expectedResp = {x: 'X'};
  const contactId = 'Alpha';

  let oldEnvVars = {};

  beforeAll(() => {
    oldEnvVars = {
      AUTOPILOT_API_KEY: process.env.AUTOPILOT_API_KEY
    };
  });

  afterAll(() => {
    process.env = {...oldEnvVars};
  });

  afterEach(() => jest.resetAllMocks());

  beforeEach(() => {
    process.env.AUTOPILOT_API_KEY = 'SomeRandomKey';
    fetch.mockImplementation(() => ({
      status: HTTPStatus.OK,
      json: () => Promise.resolve(expectedResp)
    }));
  });

  it('should pass the AUTOPILOT_API_KEY in the header "autopilotapikey" and contactId param', async () => {
    await getContact(contactId);
    expect(fetch).toHaveBeenCalledWith(
      `${AUTOPILOT_API_BASE_URL}/v1/contact/${contactId}`, {
        headers: {
          [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY,
          'Content-Type': 'application/json'
        },
        method: 'GET'
      }
    );
  });

  it('should pass the downstream response back', async () => {
    const result = await getContact(contactId);
    expect(result).toEqual({
      status: HTTPStatus.OK,
      body: expectedResp
    });
  });
});