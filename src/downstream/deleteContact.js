import fetch from 'node-fetch';

import { AUTOPILOT_API_BASE_URL, AUTOPILOT_HEADER } from '../constants';
import logger from '../utils/logger';

const deleteContact = async contactId => {

  const downstreamResp = await fetch(
    `${AUTOPILOT_API_BASE_URL}/v1/contact/${contactId}`, {
      method: 'DELETE',
      headers: {
        [AUTOPILOT_HEADER]: process.env.AUTOPILOT_API_KEY
      }
    }
  );
  logger.debug(`Downstream returned status ${downstreamResp.status} for DELETE`);
  return downstreamResp.status;
};

export default deleteContact;
