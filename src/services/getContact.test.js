import HTTPStatus from 'http-status';

import getContactService from './getContact';
import getContact from '../downstream/getContact';

jest.mock('../downstream/getContact');

describe('getContactService', () => {
  const req = {
    log: {
      info: jest.fn()
    },
    params: {
      contactId: '1'
    },
    method: 'GET'
  };

  let res = {};

  const downstreamResp = {
    status: HTTPStatus.OK,
    body: {
      FirstName: 'Geralt',
      LastName: 'ofRivia',
      Email: 'GeralOfRivia@kaermohen.org'
    }
  };

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res)
    };

    getContact.mockImplementation(() => downstreamResp);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should pass the request param to downstream getContact and response back to upstream', async () => {
    await getContactService(req, res);
    expect(getContact).toHaveBeenCalledWith(req.params.contactId);
    expect(res.send).toHaveBeenCalledWith(downstreamResp.body);
    expect(res.status).toHaveBeenCalledWith(downstreamResp.status);
  });
});
