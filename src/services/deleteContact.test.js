import HTTPStatus from 'http-status';

import deleteContactService from './deleteContact';
import deleteContact from '../downstream/deleteContact';

jest.mock('../downstream/deleteContact');

describe('deleteContactService', () => {
  const req = {
    log: {
      info: jest.fn()
    },
    params: {
      contactId: '1'
    },
    method: 'DELETE'
  };

  let res = {};

  const downstreamResp = HTTPStatus.OK;

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res)
    };

    deleteContact.mockImplementation(() => downstreamResp);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should pass the request param to downstream deleteContact and response back to upstream', async () => {
    await deleteContactService(req, res);
    expect(deleteContact).toHaveBeenCalledWith(req.params.contactId);
    expect(res.send).toHaveBeenCalledWith();
    expect(res.status).toHaveBeenCalledWith(downstreamResp);
  });
});
