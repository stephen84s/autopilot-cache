import deleteContact from '../downstream/deleteContact';

const deleteContactService = async (req, res) => {
  const contactId = req.params.contactId;
  req.log.info(`Received request to delete contact ${contactId}`);

  const result = await deleteContact(contactId);
  return res.status(result).send();
};

export default deleteContactService;
