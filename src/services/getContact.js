import getContact from '../downstream/getContact';

const getContactService = async (req, res) => {
  const {contactId} = req.params;
  const result = await getContact(contactId);
  return res.status(result.status).send(result.body);
};

export default getContactService;