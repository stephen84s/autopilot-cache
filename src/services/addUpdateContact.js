import addUpdateContact from '../downstream/addUpdateContact';

const addUpdateContactService = async (req, res) => {
  req.log.info('Received request to add / update contact');
  const result = await addUpdateContact(req.body);
  return res.status(result.status).send(result.body);
};

export default addUpdateContactService;