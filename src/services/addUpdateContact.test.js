import HTTPStatus from 'http-status';

import addUpdateContactService from './addUpdateContact';
import addUpdateContact from '../downstream/addUpdateContact';

jest.mock('../downstream/addUpdateContact');

describe('addUpdateContactService', () => {
  const req = {
    log: {
      info: jest.fn()
    },
    body: {
      FirstName: 'Steve',
      LastName: 'Rogers'
    }
  };

  let res = {};

  const downstreamResp = {
    status: HTTPStatus.OK,
    body: {
      A: 'Alpha'
    }
  };

  beforeEach(() => {
    res = {
      status: jest.fn().mockImplementation(() => res),
      send: jest.fn().mockImplementation(() => res)
    };

    addUpdateContact.mockImplementation(() => downstreamResp);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should pass the request body to downstream addUpdateContact and response back to upstream', async () => {
    await addUpdateContactService(req, res);
    expect(addUpdateContact).toHaveBeenCalledWith(req.body);
    expect(res.send).toHaveBeenCalledWith(downstreamResp.body);
    expect(res.status).toHaveBeenCalledWith(downstreamResp.status);
  });
});
