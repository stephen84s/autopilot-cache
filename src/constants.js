export const DEFAULT_PORT = 3000;
export const APPLICATION = 'autopilot-cache';
export const AUTOPILOT_API_BASE_URL = 'https://api2.autopilothq.com';
export const AUTOPILOT_HEADER = 'autopilotapikey';