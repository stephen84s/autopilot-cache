import redis from 'async-redis';
import logger from './logger';

const { REDIS_HOST, REDIS_PORT } = process.env;
const redisClient = redis.createClient({host: REDIS_HOST, port: REDIS_PORT});

redisClient.on('connect', () => {
  logger.info(`Connected to redis server at ${REDIS_HOST}:${REDIS_PORT}`);
});

redisClient.on('error', err => {
  logger.error(`Error connecting to redis server at ${REDIS_HOST}:${REDIS_PORT}, error: ${err}`);
});

export default redisClient;

