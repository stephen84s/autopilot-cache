import bunyan from 'bunyan';
import { APPLICATION } from '../constants'; 

const createLogger = () => {
  return bunyan.createLogger({
    name: APPLICATION,
    streams: [{ stream: process.stdout, level: process.env.LOG_LEVEL || 'error' }]
  });
};

const logger = createLogger();
export default logger;