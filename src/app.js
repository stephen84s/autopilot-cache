import createServer from './server';
import logger from './utils/logger';
import { APPLICATION, DEFAULT_PORT } from './constants';

const port = process.env.PORT || DEFAULT_PORT;
const server = createServer({logger});

server.on('error', e => {
  logger.error(`${APPLICATION} could not be started because of ${e.message}`);
});

server.listen(port, () => {
  logger.info(`${APPLICATION} started on port ${port}`);
});
