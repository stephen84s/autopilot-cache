import express from 'express';

import getContact from './services/getContact';
import deleteContact from './services/deleteContact';
import addUpdateContact from './services/addUpdateContact';
import cacher from './middleware/cacher/cacher';

const router = express.Router();

router.get('/contact/:contactId', cacher(req => req.params.contactId), getContact);
router.delete('/contact/:contactId', cacher(req => req.params.contactId), deleteContact);
router.post('/contact', cacher(body => body.contact_id), addUpdateContact);

export default router;