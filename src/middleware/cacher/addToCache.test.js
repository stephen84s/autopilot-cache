import addToCache from './addToCache';
import redis from '../../utils/redis';

jest.mock('../../utils/redis', () => ({
  set: jest.fn()
}));


describe('addToCache', () => {

  beforeEach(() => {
    redis.set.mockImplementation(() => Promise.resolve(1));
  });

  it('should stringify the JSON object and store against the key', async () => {
    const key = 'SomeRandomContactId';
    const batman = {
      FirstName: 'Bruce',
      Email: 'bruce@wayne.com'
    };
    await addToCache({key, respToCache: batman});
    expect(redis.set).toHaveBeenCalledWith([
      key,
      JSON.stringify({
        cached: true,
        ...batman
      })]);
  });
});

