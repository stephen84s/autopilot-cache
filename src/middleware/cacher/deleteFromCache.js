import redis from '../../utils/redis';
import logger from '../../utils/logger';

const deleteFromCache = async key => {
  const value = await redis.del(key);
  logger.debug(`Status of delete: ${value}`);
};

export default deleteFromCache;