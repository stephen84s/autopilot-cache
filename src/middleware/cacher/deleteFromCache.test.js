import deleteFromCache from './deleteFromCache';
import redis from '../../utils/redis';

jest.mock('../../utils/redis', () => ({
  del: jest.fn()
}));


describe('deleteFromCache', () => {

  beforeEach(() => {
    redis.del.mockImplementation(() => Promise.resolve(1));
  });

  it('should invoke the del function on redis client with the key', async () => {
    const key = 'SomeRandomContactId';
    await deleteFromCache(key);
    expect(redis.del).toHaveBeenCalledWith(key);
  });
});

