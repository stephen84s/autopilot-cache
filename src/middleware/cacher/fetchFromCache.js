import redis from '../../utils/redis';
import logger from '../../utils/logger';

const fetchFromCache = async key => {
  const value = await redis.get(key);
  if(value) {
    logger.debug(`Found key in cache, key: ${key}, value: ${value}`);
    return JSON.parse(value);
  }

  logger.debug(`Cache miss for key ${key}`);
  return null;
};

export default fetchFromCache;