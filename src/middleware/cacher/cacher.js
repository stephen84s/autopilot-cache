import HTTPStatus from 'http-status';

import fetchFromCache from './fetchFromCache';
import deleteFromCache from './deleteFromCache';
import addToCache from './addToCache';
import logger from '../../utils/logger';

const cacher = getKey => {
  return async (req, res, next) => {
    if (req.method === 'GET') {
      const key = getKey(req);
      logger.debug(`Search for ${key} in cache`);

      const cachedValue = await fetchFromCache(key);
      if (!cachedValue) {
        logger.debug(`${key} not found in cache`);
        decorateResponseAddToCache(res);
        return next();
      }
      return res.status(HTTPStatus.OK).send(cachedValue);
    }
    else if (req.method === 'POST') {
      decorateResponseDeleteFromCache({getKey: body => getKey(body), res});
    } else if (req.method === 'DELETE') {
      decorateResponseDeleteFromCache({getKey: () => getKey(req), res});
    }

    return next();
  };
};

const decorateResponseAddToCache = (response) => {
  const origSend = response.send.bind(response);
  response.send = body => {
    if (response.statusCode === HTTPStatus.OK) {

      logger.debug(`Adding ${body.contact_id} to the cache`);
      addToCache({key: body.contact_id, respToCache: body});

      if (body.Email) {
        logger.debug(`Adding ${body.Email} to the cache`);
        addToCache({key: body.Email, respToCache: body});
      }
    }

    response.send = origSend;
    return response.send(body);
  };
};

const decorateResponseDeleteFromCache = ({getKey, res}) => {
  const origSend = res.send.bind(res);
  res.send = async body => {
    const key = getKey(body);
    const { OK, CREATED } = HTTPStatus;
    if ([OK, CREATED].includes(res.statusCode)) {

      const contact = await fetchFromCache(key);

      if (contact) {
        logger.debug(`Removing ${contact.Email} from the cache`);
        deleteFromCache(contact.Email);

        logger.debug(`Removing ${contact.contact_id} from the cache`);
        deleteFromCache(contact.contact_id);
      }
    }

    res.send = origSend;
    return res.send(body);
  };
};

export default cacher;