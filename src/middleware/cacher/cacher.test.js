import HTTPStatus from 'http-status';
import cacher from './cacher';

import fetchFromCache from './fetchFromCache';
import deleteFromCache from './deleteFromCache';
import addToCache from './addToCache';

jest.mock('./fetchFromCache');
jest.mock('./deleteFromCache');
jest.mock('./addToCache');
jest.mock('../../utils/redis');
jest.mock('async-redis', () => ({
  createClient: () => ({
    on: jest.fn()
  })
}));



describe('index', () => {
  const req = {
    params: {},
    body: {}
  };

  const res = {

  };

  const next = jest.fn();

  afterEach(() => jest.resetAllMocks());

  describe('when request is a GET', () => {
    const getContactReq = {
      ...req,
      params: {
        contactId: 'JamesBond'
      },
      method: 'GET'
    };
    describe('when the contact is not present in the cache', () => {

      beforeEach(() => {
        fetchFromCache.mockImplementation(() => Promise.resolve(null));
      });

      describe('when the request was not successfull', () => {
        const notSuccessResp = {
          ...res,
          statusCode: HTTPStatus.BAD_REQUEST,
          send: jest.fn().mockImplementation(() => notSuccessResp)
        };

        const expectedBody = {x: 'SomeRandom'};

        it('should not save the response in the cache', async () => {
          const cacherMiddleware = cacher(req => req.params.contactId);
          const notSuccessRespSend = notSuccessResp.send;

          expect(notSuccessResp.send).toBe(notSuccessRespSend);
          await cacherMiddleware(getContactReq, notSuccessResp, next);

          // send should be decorated now
          expect(notSuccessResp.send).not.toBe(notSuccessRespSend);


          // Not in cache so next should be called
          expect(next).toHaveBeenCalledWith();

          expect(notSuccessResp.send).not.toBe(notSuccessRespSend);
          notSuccessResp.send(expectedBody);

          expect(notSuccessResp.send()).toEqual(notSuccessResp);
        });
      });

      describe('when the request is successfull', () => {
        const successBody = {
          'FirstName': 'Tony',
          'LastName': 'Stark',
          'Email': 'tony@stark.com',
          'contact_id': 'SomeID'
        };
        const successResp = {
          statusCode: HTTPStatus.OK,
          send: jest.fn().mockImplementation(() => successResp)
        };

        it('should store the response in the cache', async () => {
          const cacherMiddleware = cacher(req => req.params.contactId);
          const successRespSend = successResp.send;

          expect(successResp.send).toBe(successRespSend);
          await cacherMiddleware(getContactReq, successResp, next);

          // send should be decorated now
          expect(successResp.send).not.toBe(successRespSend);

          // Not in cache so next should be called
          expect(next).toHaveBeenCalledWith();

          successResp.send(successBody);
          expect(addToCache).toHaveBeenNthCalledWith(
            1, {
              key: successBody.contact_id,
              respToCache: successBody
            });
          expect(addToCache).toHaveBeenNthCalledWith(
            2, {
              key: successBody.Email,
              respToCache: successBody
            });
        });
      });
    });
    describe('when the contact is found in cache', () => {
      const contactBody = {
        'FirstName': 'Tony',
        'LastName': 'Stark',
        'Email': 'tony@stark.com',
        'contact_id': 'SomeID'
      };
      let cachedResp = {};

      beforeEach(() => {
        cachedResp = {
          ...res,
          status: jest.fn().mockImplementation(() => cachedResp),
          send: jest.fn().mockImplementation(() => cachedResp)
        };
        fetchFromCache.mockImplementation(() => Promise.resolve(contactBody));     
      });

      it('should return the contact from the cache', async () => {
        const cacherMiddleware = cacher(req => req.params.contactId);
        const successRespSend = cachedResp.send;

        await cacherMiddleware(getContactReq, cachedResp, next);

        // Response is not decorated this time
        expect(cachedResp.send).toBe(successRespSend);

        // In cache so next should not be called
        expect(next).toHaveBeenCalledTimes(0);
        expect(addToCache).toHaveBeenCalledTimes(0);
        expect(cachedResp.status).toHaveBeenCalledWith(HTTPStatus.OK);
        expect(cachedResp.send).toHaveBeenCalledWith(contactBody);
        expect(cachedResp.send()).toEqual(cachedResp);

      });
    });
  });
  describe('when request is a POST (add/edit contact)', () => {
    const contact = {
      'FirstName': 'Tony',
      'LastName': 'Stark',
      'Email': 'tony@stark.com',
    };
    
    const addUdateContactReq = {
      ...req,
      body: {
        contact
      },
      method: 'POST'
    };

    describe('when the request is successful', () => {

      const successResp = {
        ...res,
        statusCode: HTTPStatus.OK,
        send: jest.fn().mockImplementation(() => successResp)
      };

      const successRespBody = {
        contact_id: 'SomeRandomID'
      };

      beforeEach(() => {
        fetchFromCache.mockImplementation(() => Promise.resolve({
          ...contact,
          ...successRespBody
        }));     
      });

      it('should delete the contact from the cache', async () => {
        const cacherMiddleware = cacher(body => body.contact_id);
        const successRespSend = successResp.send;

        expect(successResp.send).toBe(successRespSend);
        await cacherMiddleware(addUdateContactReq, successResp, next);

        // send should be decorated now
        expect(successResp.send).not.toBe(successRespSend);
        expect(next).toHaveBeenCalledWith();

        await successResp.send(successRespBody);
        expect(deleteFromCache).toHaveBeenNthCalledWith(1, contact.Email);
        expect(deleteFromCache).toHaveBeenNthCalledWith(2, successRespBody.contact_id);
      });
    });

    describe('when the request is not successful', () => {

      const failedResp = {
        ...res,
        statusCode: HTTPStatus.BAD_REQUEST,
        send: jest.fn().mockImplementation(() => failedResp)
      };

      const failedRespBody = {
        someId: 'SomeId'
      };

      it('should not delete the contact from the cache', async () => {
        const cacherMiddleware = cacher(body => body.contact_id);
        const failedRespSend = failedResp.send;

        expect(failedResp.send).toBe(failedRespSend);
        await cacherMiddleware(addUdateContactReq, failedResp, next);

        expect(failedResp.send).not.toBe(failedRespSend);
        expect(next).toHaveBeenCalledWith();

        await failedResp.send(failedRespBody);
        expect(deleteFromCache).toHaveBeenCalledTimes(0);
      });
    });
  });
  describe('when request is a DELETE', () => {

    const deleteContactReq = {
      ...req,
      params: {
        contactId: 'JamesBond'
      },
      method: 'DELETE'
    };

    describe('when the request is successful', () => {
      const contact = {
        FirstName: 'Tony',
        LastName: 'Stark',
        Email: 'tony@stark.com',
        contact_id: 'ARandomId'
      };

      const successResp = {
        ...res,
        statusCode: HTTPStatus.OK,
        send: jest.fn().mockImplementation(() => successResp)
      };

      beforeEach(() => {
        fetchFromCache.mockImplementation(() => Promise.resolve(contact));
      });

      it('should delete the contact from the cache', async () => {
        const cacherMiddleware = cacher(body => body.contact_id);
        const successRespSend = successResp.send;

        expect(successResp.send).toBe(successRespSend);
        await cacherMiddleware(deleteContactReq, successResp, next);

        // send should be decorated now
        expect(successResp.send).not.toBe(successRespSend);
        expect(next).toHaveBeenCalledWith();

        await successResp.send();
        expect(deleteFromCache).toHaveBeenNthCalledWith(1, contact.Email);
        expect(deleteFromCache).toHaveBeenNthCalledWith(2, contact.contact_id);
      });
    });

    describe('when the request is not successful', () => {

      const failedResp = {
        ...res,
        statusCode: HTTPStatus.BAD_REQUEST,
        send: jest.fn().mockImplementation(() => failedResp)
      };

      const failedRespBody = {
        someId: 'SomeId'
      };

      it('should not delete the contact from the cache', async () => {
        const cacherMiddleware = cacher(body => body.contact_id);
        const failedRespSend = failedResp.send;

        expect(failedResp.send).toBe(failedRespSend);
        await cacherMiddleware(deleteContactReq, failedResp, next);

        expect(failedResp.send).not.toBe(failedRespSend);
        expect(next).toHaveBeenCalledWith();

        await failedResp.send(failedRespBody);
        expect(deleteFromCache).toHaveBeenCalledTimes(0);
        expect(failedRespSend).toHaveBeenCalledWith(failedRespBody);
      });
    });
   
  });
});
