import redis from '../../utils/redis';
import logger from '../../utils/logger';

const addToCache = async ({key, respToCache}) => {
  logger.debug(`Adding to cache, key: ${key}, value: ${JSON.stringify(respToCache)}`);
  await redis.set([key, JSON.stringify({cached: true, ...respToCache})]);
};

export default addToCache;
