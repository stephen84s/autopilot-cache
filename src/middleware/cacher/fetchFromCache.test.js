import fetchFromCache from './fetchFromCache';
import redis from '../../utils/redis';

jest.mock('../../utils/redis', () => ({
  get: jest.fn()
}));


describe('fetchFromCache', () => {
  const testKey = 'TEST_KEY';

  describe('when key is not found in the cache', () => {
    beforeEach(() => {
      redis.get.mockImplementation(() => Promise.resolve(null));
    });
    
    it('should return null', async () => {
      const value = await fetchFromCache(testKey);
      expect(value).toBeNull();
      expect(redis.get).toHaveBeenCalledWith(testKey);
    });
  });

  describe('when the value exists in the map', () => {
    const expectedValue = {
      'FirstName': 'James',
      'LastName': 'Bond',
      'Email': 'jamesmbond@mi6.com'
    };
    beforeEach(() => {
      redis.get.mockImplementation(() => Promise.resolve(JSON.stringify(expectedValue)));
    });
  
    it('should invoke the del function on redis client with the key', async () => {
      const key = 'SomeRandomContactId';
      const fetchedValue = await fetchFromCache(key);
      expect(fetchedValue).toEqual(expectedValue);
      expect(redis.get).toHaveBeenCalledWith(key);
    });
  });
});

