FROM node:11.9.0 AS base

RUN mkdir /app
WORKDIR  /app
COPY . .

RUN yarn
RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start" ]